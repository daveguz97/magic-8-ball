// Declare my variables
const magic8Ball = document.querySelector('#magic-8-ball');
const triangle = document.querySelector('#triangle');
const textInput = document.querySelector('#text-input').value;
const form = document.querySelector('form');
const btn = document.querySelector('.btn');
let ansArr = [
  ' It is certain.',
  'It is decidedly so.',
  'Without a doubt.',
  'Yes - definitely.',
  'You may rely on it.',
  'As I see it, yes.',
  'Most likely.',
  'Outlook good.',
  'Yes.',
  'Signs point to yes.',
  'Reply hazy, try again.',
  'Ask again later.',
  'Better not tell you now.',
  'Cannot predict now.',
  'Concentrate and ask again.',
  "Don't count on it.",
  'My reply is no.',
  'My sources say no.',
  'Outlook not so good.',
  'Very doubtful.'
];
const p = document.createElement('p');
p.className = 'answers';
triangle.appendChild(p);
let text = document.createTextNode(getAnswers());

// Add an event Listener for when the button is clicked
btn.addEventListener('click', onClick);

function onClick(e) {
  if (form.checkValidity() === false) {
    return false;
  }
  magic8Ball.className = 'on-click';
  text.textContent = getAnswers();
  p.appendChild(text);
  setTimeout(function() {
    magic8Ball.classList.remove('on-click');
  }, 600);
  e.preventDefault();
}

// Returns a random answer
function getAnswers() {
  ans = ansArr[Math.floor(Math.random() * ansArr.length)];
  return ans;
}
